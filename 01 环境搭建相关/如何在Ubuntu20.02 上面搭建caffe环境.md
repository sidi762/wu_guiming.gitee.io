# 如何在Ubuntu20.02 上面搭建caffe环境

## 1、搭建opencv3.4的环境

### 步骤1：获取opencv3.4版本软件包

OpenCV下载地址：

官网：https://opencv.org/

Github：https://github.com/opencv/

或直接下载

```
链接：https://pan.baidu.com/s/1VL6_VMCp0I4-y9DF_SOzJg 
提取码：inx8 
```

### 步骤2：将下载的opencv软件包拷贝至Ubuntu

* 关于如何将Windows的文件拷贝至Ubuntu，可以自行百度，也可以参考《[如何在Ubuntu配置Samba服务](https://blog.csdn.net/Wu_GuiMing/article/details/115031721?spm=1001.2014.3001.5501)》，博客链接中涉及到的所有的hispark都是作者自己的Ubuntu的用户名，请您根据自己的用户名自行配置。

### 步骤3：解压opencv软件包

* 在Ubuntu的opencv压缩包所在的路径下，执行下面的命令，对压缩包进行解压缩

```
unzip opencv-3.4.16.zip
```

![image-20220426104535135](pic/image-20220426104535135.png)

### 步骤4：下载编译opencv所需要的软件

* 在Ubuntu终端，分步执行下面的命令，下载编译Opencv时所需要的软件

```
sudo add-apt-repository "deb http://security.ubuntu.com/ubuntu xenial-security main"
sudo apt update -y
sudo apt upgrade -y
sudo apt-get install build-essential libgtk2.0-dev libavcodec-dev cmake -y
sudo apt-get install libavformat-dev libjpeg.dev libtiff5.dev libswscale-dev libjasper-dev -y
```

![image-20220426104843555](pic/image-20220426104843555.png)

### 步骤5：编译opencv

* 进行opencv-3.4.16目录下，分步执行下面的命令，编译opencv，编译opencv需要一定的时间，请耐心等待

```
mkdir build

cd build

cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr/local ..

sudo make -j4
```

![image-20220426105653442](pic/image-20220426105653442.png)

![image-20220426105730180](pic/image-20220426105730180.png)

![image-20220426111735790](pic/image-20220426111735790.png)

### 步骤6：安装opencv

* 执行下面的命令，安装opencv到Ubuntu系统

```
sudo make install
```

![image-20220426111830480](pic/image-20220426111830480.png)

### 步骤7：配置opencv的环境变量

* 将opencv的库添加到 /etc/ld.so.conf.d/opencv.conf文件中

* 执行下面的命令，打开/etc/ld.so.conf.d/opencv.conf文件

```
sudo gedit /etc/ld.so.conf.d/opencv.conf
```

![image-20220426112130191](pic/image-20220426112130191.png)

* 在opencv.conf文件中添加下面这行命令，然后点击保存

```
/usr/local/lib
```

![image-20220426112312709](pic/image-20220426112312709.png)

* 保存后，会弹出下面的warning，可以忽略

![image-20220426112330830](pic/image-20220426112330830.png)

* 执行下面的命令，更新一下系统共享链接库

```
sudo ldconfig
```

![image-20220426112529890](pic/image-20220426112529890.png)

* 执行下面命令，打开bash.bashrc，添加opencv的路径至Ubuntu的环境变量

```
sudo gedit /etc/bash.bashrc 
```

![image-20220426112636909](pic/image-20220426112636909.png)

* 在bash.bashrc 文件中添加下面这两行命令至文档的末尾，然后点击保存

```
PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/lib/pkgconfig  
export PKG_CONFIG_PATH  
```

![image-20220426113015002](pic/image-20220426113015002.png)

* 执行下面的命令，使上面的配置生效

```
source /etc/bash.bashrc
```

![image-20220426113253280](pic/image-20220426113253280.png)

* 执行下面的命令，验证opencv的环境是否搭建成功。

```
pkg-config --modversion opencv
```

* 如下图所示，如果显示opencv的版本号，就说明opencv的环境已经搭建成功了

![image-20220426113353898](pic/image-20220426113353898.png)

## 2、Caffe环境搭建

* 1、在Ubuntu系统中，分步执行下面的命令，对Ubuntu里面的软件进行更新。

```sh
sudo apt-get update
sudo apt-get upgrade
```

* 2、分步执行下面的命令，安装所需的依赖软件。

```sh
sudo apt-get install  build-essential cmake git pkg-config -y
sudo apt-get install libprotobuf-dev libleveldb-dev libsnappy-dev libhdf5-serial-dev protobuf-compiler -y
sudo apt-get install  liblapack-dev -y
sudo apt-get install  libatlas-base-dev -y
sudo apt-get install --no-install-recommends libboost-all-dev -y
sudo apt-get install  libgflags-dev libgoogle-glog-dev liblmdb-dev -y
sudo apt-get install python3-pip -y
sudo apt-get install python3-numpy python3-scipy -y
```

* 3、执行下面的命令，下载caffe开源软件

```sh
git clone https://github.com/BVLC/caffe.git
```

* 4、进入caffe/python/目录下，执行下面的命令，下载依赖的软件

```sh
cd caffe/python
for req in $(cat requirements.txt); do pip3 install $req; done
```

* 在下载过程中如果出现下面的error或者warning，可以暂时忽略

![image-20220426115307905](pic/image-20220426115307905.png)

* 5、执行下面的命令，将 Makefile.config.example 文件复制一份并更名为 Makefile.config

```sh
cd ..

cp Makefile.config.example Makefile.config
```

* 6、接下来是修改Makefile.config里面的配置,使用gedit工具打开Makefile.config，修改之后点击保存。

```sh
gedit Makefile.config
```

* ① 将CPU_ONLY前面的注释去掉。

```sh
# 将 
# CPU_ONLY := 1
# 改为
CPU_ONLY := 1
```

* ② 将OPENCV_VERSION前面的注释去掉

```sh
# 将
# OPENCV_VERSION := 3
# 改为
OPENCV_VERSION := 3
```

* ③ 因为我们Ubuntu的python版本是python3.8，所以请把PYTHON_INCLUDE = python2.7这个配置前面加上注释，且把PYTHON_INCLUDE=python3.5的注释打开，把所有的3.5都改成3.8，具体修改如下：

![image-20220426115838181](pic/image-20220426115838181.png)

* ④ 将WITH_PYTHON_LAYER := 1前面的注释去掉

```sh
# 将 
# WITH_PYTHON_LAYER := 1
# 改为
WITH_PYTHON_LAYER := 1
```

* ⑤ 修改INCLUDE_DIRS和LIBRARY_DIRS

```sh
# 将 
INCLUDE_DIRS := $(PYTHON_INCLUDE) /usr/local/include
LIBRARY_DIRS := $(PYTHON_LIB) /usr/local/lib /usr/lib
改为
INCLUDE_DIRS := $(PYTHON_INCLUDE) /usr/local/include /usr/include/hdf5/serial
LIBRARY_DIRS := $(PYTHON_LIB) /usr/local/lib /usr/lib usr/lib/x86_64-linux-gnu /usr/lib/x86_64-linux-gnu/hdf5/serial
```

修改后的文件如下所示：

```python
## Refer to http://caffe.berkeleyvision.org/installation.html
# Contributions simplifying and improving our build system are welcome!

# cuDNN acceleration switch (uncomment to build with cuDNN).
# USE_CUDNN := 1

# CPU-only switch (uncomment to build without GPU support).
CPU_ONLY := 1

# uncomment to disable IO dependencies and corresponding data layers
# USE_OPENCV := 0
# USE_LEVELDB := 0
# USE_LMDB := 0
# This code is taken from https://github.com/sh1r0/caffe-android-lib
# USE_HDF5 := 0

# uncomment to allow MDB_NOLOCK when reading LMDB files (only if necessary)
#	You should not set this flag if you will be reading LMDBs with any
#	possibility of simultaneous read and write
# ALLOW_LMDB_NOLOCK := 1

# Uncomment if you're using OpenCV 3
OPENCV_VERSION := 3

# To customize your choice of compiler, uncomment and set the following.
# N.B. the default for Linux is g++ and the default for OSX is clang++
# CUSTOM_CXX := g++

# CUDA directory contains bin/ and lib/ directories that we need.
CUDA_DIR := /usr/local/cuda
# On Ubuntu 14.04, if cuda tools are installed via
# "sudo apt-get install nvidia-cuda-toolkit" then use this instead:
# CUDA_DIR := /usr

# CUDA architecture setting: going with all of them.
# For CUDA < 6.0, comment the *_50 through *_61 lines for compatibility.
# For CUDA < 8.0, comment the *_60 and *_61 lines for compatibility.
# For CUDA >= 9.0, comment the *_20 and *_21 lines for compatibility.
CUDA_ARCH := -gencode arch=compute_20,code=sm_20 \
		-gencode arch=compute_20,code=sm_21 \
		-gencode arch=compute_30,code=sm_30 \
		-gencode arch=compute_35,code=sm_35 \
		-gencode arch=compute_50,code=sm_50 \
		-gencode arch=compute_52,code=sm_52 \
		-gencode arch=compute_60,code=sm_60 \
		-gencode arch=compute_61,code=sm_61 \
		-gencode arch=compute_61,code=compute_61

# BLAS choice:
# atlas for ATLAS (default)
# mkl for MKL
# open for OpenBlas
BLAS := atlas
# Custom (MKL/ATLAS/OpenBLAS) include and lib directories.
# Leave commented to accept the defaults for your choice of BLAS
# (which should work)!
# BLAS_INCLUDE := /path/to/your/blas
# BLAS_LIB := /path/to/your/blas

# Homebrew puts openblas in a directory that is not on the standard search path
# BLAS_INCLUDE := $(shell brew --prefix openblas)/include
# BLAS_LIB := $(shell brew --prefix openblas)/lib

# This is required only if you will compile the matlab interface.
# MATLAB directory should contain the mex binary in /bin.
# MATLAB_DIR := /usr/local
# MATLAB_DIR := /Applications/MATLAB_R2012b.app

# NOTE: this is required only if you will compile the python interface.
# We need to be able to find Python.h and numpy/arrayobject.h.
# PYTHON_INCLUDE := /usr/include/python2.7 \
#		/usr/lib/python2.7/dist-packages/numpy/core/include
# Anaconda Python distribution is quite popular. Include path:
# Verify anaconda location, sometimes it's in root.
# ANACONDA_HOME := $(HOME)/anaconda
# PYTHON_INCLUDE := $(ANACONDA_HOME)/include \
		# $(ANACONDA_HOME)/include/python2.7 \
		# $(ANACONDA_HOME)/lib/python2.7/site-packages/numpy/core/include

# Uncomment to use Python 3 (default is Python 2)
PYTHON_LIBRARIES := boost_python38 python3.8
PYTHON_INCLUDE := /usr/include/python3.8 \
                 /usr/lib/python3/dist-packages/numpy/core/include

# We need to be able to find libpythonX.X.so or .dylib.
PYTHON_LIB := /usr/lib
# PYTHON_LIB := $(ANACONDA_HOME)/lib

# Homebrew installs numpy in a non standard path (keg only)
# PYTHON_INCLUDE += $(dir $(shell python -c 'import numpy.core; print(numpy.core.__file__)'))/include
# PYTHON_LIB += $(shell brew --prefix numpy)/lib

# Uncomment to support layers written in Python (will link against Python libs)
WITH_PYTHON_LAYER := 1

# Whatever else you find you need goes here.
INCLUDE_DIRS := $(PYTHON_INCLUDE) /usr/local/include /usr/include/hdf5/serial
LIBRARY_DIRS := $(PYTHON_LIB) /usr/local/lib /usr/lib usr/lib/x86_64-linux-gnu /usr/lib/x86_64-linux-gnu/hdf5/serial

# If Homebrew is installed at a non standard location (for example your home directory) and you use it for general dependencies
# INCLUDE_DIRS += $(shell brew --prefix)/include
# LIBRARY_DIRS += $(shell brew --prefix)/lib

# NCCL acceleration switch (uncomment to build with NCCL)
# https://github.com/NVIDIA/nccl (last tested version: v1.2.3-1+cuda8.0)
# USE_NCCL := 1

# Uncomment to use `pkg-config` to specify OpenCV library paths.
# (Usually not necessary -- OpenCV libraries are normally installed in one of the above $LIBRARY_DIRS.)
# USE_PKG_CONFIG := 1

# N.B. both build and distribute dirs are cleared on `make clean`
BUILD_DIR := build
DISTRIBUTE_DIR := distribute

# Uncomment for debugging. Does not work on OSX due to https://github.com/BVLC/caffe/issues/171
# DEBUG := 1

# The ID of the GPU that 'make runtest' will use to run unit tests.
TEST_GPUID := 0

# enable pretty build (comment to see full commands)
Q ?= @
```

* 7、修改Makefile文件里面的一些配置，使用gedit 打开Makefile，进行修改，修改完成后，点击保存并关闭文件

```
gedit Makefile
```

* ① 修改DYNAMIC_VERSION_REVISION的值

```sh
# 将
DYNAMIC_VERSION_REVISION  := 0
# 改为
DYNAMIC_VERSION_REVISION  := 0-rc3
```

* ② 修改LIBRARIES的值

```sh
# 将
LIBRARIES += glog gflags protobuf boost_system boost_filesystem m
# 改为
LIBRARIES += glog gflags protobuf boost_system boost_filesystem boost_regex m hdf5_hl hdf5
```

![](pic/029%E4%BF%AE%E6%94%B9libraries.png)

```sh
# 将
LIBRARIES += opencv_imgcodecs
# 改为
LIBRARIES += opencv_imgcodecs opencv_videoio
```

![](pic/030%E4%BF%AE%E6%94%B9libraries2.png)

* ③ 将# NCCL acceleration configuration下面的四行注释掉

```sh
# 将
# NCCL acceleration configuration
ifeq ($(USE_NCCL), 1)

    LIBRARIES += nccl
    COMMON_FLAGS += -DUSE_NCCL
endif
# 改为
# NCCL acceleration configuration
# ifeq ($(USE_NCCL), 1)
#   LIBRARIES += nccl
#   COMMON_FLAGS += -DUSE_NCCL
# endif
```

![](pic/031%E6%B3%A8%E9%87%8ANCCL.png)

* 8、在caffe目录下，分步执行下面的命令，来编译caffe。

```sh
make -j4
make pycaffe
```

* 9、执行下面的命令，将caffe的python路径设置为环境变量，并更新环境变量。

​	执行下面的命令，打开.bashrc

```sh
sudo gedit ~/.bashrc
```

把下面的命令，添加到.bashrc文件的末尾，主要/home/hispark/tool/caffe/python是我自己Ubuntu的caffe/python路径，这里请填写为您自己Ubuntu中的caffe/python路径。

```shell
在文件的末尾加上下面的语句
export PYTHONPATH=/home/hispark/tool/caffe/python:$PYTHONPATH
```

![image-20220426120846210](pic/image-20220426120846210.png)

​	在执行下面的命令，更新环境变量

```sh
source ~/.bashrc
```

* 10、测试caffe环境是否OK，在Ubuntu的任意目录下，执行 python3，当出现”>>>”的提示符后，再输入import caffe，如果没有任何报错信息，说明caffe环境已经搭建成功了。

```sh
python3
import caffe
```

![image-20220426120954508](pic/image-20220426120954508.png)