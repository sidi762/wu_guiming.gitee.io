# 如何在训练服务中进行pytorch2caffe模型的转换

<font color='RedOrange'>**由于我们的训练服务器已经搭建了torch环境，其中 python 版本为 3.6.5，CUDA 11.3 对应的 torch 版本为 1.10.2，torchvision 版本为 0.11.3，如果按照《PytorchCaffe 方案》在自己的Ubuntu进行环境搭建的话，可能会因为训练出来的模型的torch版本太高，导致无法进行模型的转换**</font>

* 针对上面问题，我们给出的解决方案是在训练服务中搭建pytorch2caffe的环境

## 步骤1：下载pytorch_to_caffe_master

* 访问下面的链接，下载pytorch_to_caffe_master压缩包

```
链接：https://pan.baidu.com/s/1635aEoSnq86x4aWGB2Yjqg  提取码：7bit 
```



* 使用MobaXterm工具，登录好自己队伍专属的训练服务器账号，然后把刚下载好的pytorch_to_caffe_master压缩包，上传至训练服务中

![1655125294214](pic/1655125294214.png)



* 使用下面的命令，对pytorch_to_caffe_master压缩包进行解压

```
tar -zxvf pytorch_to_caffe_master.tgz
```

![1655125363874](pic/1655125363874.png)



## 步骤2：安装模型转换是依赖的软件

* 在训练服务中执行下面的命令，安装依赖软件

```
pip install google -i https://pypi.tuna.tsinghua.edu.cn/simple some-package

pip install protobuf -i https://pypi.tuna.tsinghua.edu.cn/simple some-package
```

![1655184891704](pic/1655184891704.png)



## 步骤3：修改代码

* 对pytorch_to_caffe_master/example/resnet_pytorch_2_caffe.py进行修改，具体修改如下图所示：
* 主要需要修改的是num_classes的种类，以及您pytorch模型的绝对路径
* **注意：如果您是使用NVIDIA A100 训练服务器进行训练的，你应该指定的是epoch_100.pth的路径，而不是latest.pth的路径，因为latest.pth只是epoch_100.pth的一个软连接，我们最后指定epoch_100.pth的路径就好。**

![1655185327829](pic/1655185327829.png)

## 步骤4：模型转换

* 在训练服务器的pytorch_to_caffe_master目录下，按照下面的命令进行模型转换

```
python example/resnet_pytorch_2_caffe.py
```

![1655185462341](pic/1655185462341.png)



* 转换成功后，即可在pytorch_to_caffe_master目录下生成resnet18.caffemodel和resnet18.prototxt文件

![1655185507542](pic/1655185507542.png)