# Docker编译环境使⽤指南

### 前提条件：

1、请先按照博客《[如何快速安装VMware虚拟机](https://blog.csdn.net/Wu_GuiMing/article/details/115030666?spm=1001.2014.3001.5502)》的步骤把虚拟机安装好。

2、请按照博客《[如何快速安装Ubuntu20.04](https://gitee.com/wgm2022/wu_guiming.gitee.io/blob/master/01%20%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA%E7%9B%B8%E5%85%B3/%E5%A6%82%E4%BD%95%E5%BF%AB%E9%80%9F%E5%AE%89%E8%A3%85ubuntu20.04.md)》的步骤把Ubuntu安装好。

3、<font color='RedOrange'>如果您的Ubuntu之前下载了openharmony的代码，可以把您Ubuntu中的openharmony代码删掉，避免因为Ubuntu内存太小导致的无法预测的错误。</font>

4、按照《[搭建Windows开发环境](https://device.harmonyos.com/cn/docs/documentation/guide/ide-install-windows-ubuntu-0000001194073744#section186614181716)》文档，搭建windows的开发工具。<font color='RedOrange'>（已经操作过的可以忽略）</font>

5、按照《[安装Remote SSH](https://gitee.com/link?target=https%3A%2F%2Fdevice.harmonyos.com%2Fcn%2Fdocs%2Fdocumentation%2Fguide%2Fide-install-windows-ubuntu-0000001194073744%23section11182111523710)》步骤，在IDE工具中安装Remote SSH插件。<font color='RedOrange'>（已经操作过的可以忽略）</font>

### 步骤1：安装docker软件

* 执行下面的命令，进行在Ubuntu下安装docker

```
sudo apt  install docker.io -y
```

![image-20220502115306764](pic/image-20220502115306764.png)

### 步骤2：下载docker镜像包

```
链接：https://pan.baidu.com/s/1czd_Droyrs4qP0T8c9sT7w   提取码：l7rj 
```

<font color='RedOrange'>**docker镜像包中主要包含了**</font>：

* OpenHarmony的代码，<font color='RedOrange'>在 /home/openharmony/目录下</font>
* 编译OpenHarmony代码时所依赖的编译环境。
* caffe环境，<font color='RedOrange'>在/root/caffe/目录下</font>
* opencv环境，<font color='RedOrange'>在/root/opencv/目录下</font>
* pytorch2caffe环境，<font color='RedOrange'>在/root/pytorch_to_caffe_master/目录下</font>
* darknet2caffe环境，<font color='RedOrange'>在/root/darknet2caffe/目录下</font>
* 适配了Taurus开发板，不需要再关闭媒体服务和MIPI_TX驱动、也不需要重新配置网口。
* 编译Taurus的sample时所需的资源文件也全部在docker镜像中。
* 配置了samba服务

![image-20220518150200011](pic/image-20220518150200011.png)

![image-20220518150219091](pic/image-20220518150219091.png)

### 步骤3：将docker压缩包拷贝至Ubuntu

<font color='RedOrange '>注意：如果您的虚拟机是virtualbox，或者无法按照下面的方法找到/mnt/hgfs/share文件夹，可以尝试直接把docker文件复制粘贴（**拖拽**）到Ubuntu的文件夹中。如果无法进行复制粘贴，请自行百度搜索对应的解决方案</font>。

* <font color='RedOrange '>关闭Ubuntu</font>，打开编译虚拟机设置

![image-20220507190621568](pic/image-20220507190621568.png)

* 点击选项，选择共享文件夹，点击总是启用

![image-20220507190728401](pic/image-20220507190728401.png)

* 点击添加按钮，然后会弹出欢迎的对话框，点击下一步

![image-20220507190900476](pic/image-20220507190900476.png)

* 点击浏览按钮，选择你需要共享的Windows文件夹（后面我们把需要复制到Ubuntu的文件，放到E盘的share文件夹之后，就能在Ubuntu看到对应的文件了），然后点击下一步

![image-20220507191042791](pic/image-20220507191042791.png)

* 勾选启用此共享，然后点击完成

![image-20220507191122532](pic/image-20220507191122532.png)

* 点击确定。

![image-20220507191157039](pic/image-20220507191157039.png)

* 给Ubuntu开机。

![image-20220507191422927](pic/image-20220507191422927.png)

* 等Ubuntu开机之后，输入密码，进入Ubuntu系统，在Ubuntu的桌面，鼠标右键，选择打开终端。

![image-20220507191722577](pic/image-20220507191722577.png)

* 把下载好的docker镜像放入Windows E盘的share目录下（这个根据您自己配置的Windows的共享目录有关）

![image-20220507192119043](pic/image-20220507192119043.png)

* 然后去Ubuntu的终端，执行下面的命令，就可以看到docker文件，已经在Ubuntu的share文件夹下面了。

```
cd  /mnt/hgfs/share

ls 
```

![image-20220507195840718](pic/image-20220507195840718.png)

* <font color='RedOrange '>如果您的Ubuntu之前有配置过samba服务，请执行下面的命令，关闭您Ubuntu上面的samba服务，避免和后面docker镜像中使用的samba产生冲突</font>

```
sudo service smbd stop
```

![image-20220507224621765](pic/image-20220507224621765.png)

### 步骤4：导入docker镜像

* 使用下面的命令，导入docker镜像

```
sudo docker load < embedded-race-hisilicon-2022-0.3.0-by-HonestQiao.tar.gz
```

![image-20220507231536990](pic/image-20220507231536990.png)

### 步骤5：查看docker image信息

* 使用下面的命令，查看docker image的具体信息

```
sudo docker image ls
```

![image-20220507232043427](pic/image-20220507232043427.png)

### 步骤6：启动docker编译环境

* 执行下面的命令，启动docker编译环境，其中<font color='RedOrange '>openharmony</font>为自定义的docker的名字，<font color='RedOrange '>7f917f0e7a6f</font>为我Ubuntu下docker的IMAGE ID，这里<font color='RedOrange '>请根据自己docker image ID的不同自行修改</font>。

```shell
sudo docker run -it --net=host --name openharmony -v /home/hispark/docker_share:/home/share 7f917f0e7a6f  

# sudo docker run -it --net=host --name [容器实例名] -v /home/你Ubuntu的用户名/docker_share:/home/share [镜像ID]
```

![image-20220507232142726](pic/image-20220507232142726.png)

* **<font color='RedOrange '>如果您的Ubuntu重启之后，需要再次启动dcoker编译环境的话</font>**，需要执行下面的命令

```sh
# 查看当前运⾏的docker实例状态
sudo docker ps -a

# 在上⼀条指显示结果列表中，查看openharmony的STATUS
# 如为 Exited，则需要执⾏下⾯这条指令，再次启动
# 如为 Up，则跳过下⾯这条指令
sudo docker start openharmony

# 进⼊docker编译环境
sudo docker exec -it openharmony bash
# 执⾏后，出现类似如下信息，说明再次进⼊成功
# root@bae85ba0f77c:/home/openharmony#
```

![image-20220506163644941](pic/image-20220506163644941.png)

* 如果你想<font color='RedOrange '>退出docker的编译环境</font>，需要执行下面的命令

```sh
exit
```

![image-20220507192859616](pic/image-20220507192859616.png)

### 步骤7：配置docker的samba服务，方便Windows与docker之间的文件共享

* 输入下面的命令，启动docker的samba服务，<font color='RedOrange '>且每次重新开启docker环境，最好先执行一遍这条命令</font>

  ``` 
  service smbd restart
  ```

  ![image-20220507194023345](pic/image-20220507194023345.png)

* 执行下面的命令，查看Ubuntu的IP地址，本人Ubuntu的IP地址是 192.168.174.168

```
ifconfig
```

![image-20220507171108834](pic/image-20220507171108834.png)

* 点击Windows的此电脑，鼠标右键，选择映射网络驱动器

![image-20220507194232987](pic/image-20220507194232987.png)

* 输入<font color='RedOrange '>\\\Ubuntu的IP地址\docker</font>，然后点击完成，输入账号(root)和初始密码 (123456)

```
\\192.168.174.168\docker
```

![image-20220507195034516](pic/image-20220507195034516.png)

* 这样docker的根目录就能够在Windows的磁盘下面显示了。这样您就可以方便的进行Windows和Ubuntu之间的文件共享了

![image-20220507195406786](pic/image-20220507195406786.png)



### 步骤8：使用VSCode,通过ssh与Ubuntu建立链接

* 打开VSCode，点击remote explorer，然后再点击ssh  targets右边的加号

![image-20220502153001843](pic/image-20220502153001843.png)

* 输入如下的指令，然后敲回车，默认密码是：<font color='RedOrange '>123456</font>

```
ssh root@ubuntu的ip地址 -p 52222    
```

![image-20220507172443114](pic/image-20220507172443114.png)

* 选择第一个config

![image-20220502153232993](pic/image-20220502153232993.png)

* 点击SSH TARGETS下面IP地址 右边的+号

![image-20220507172536383](pic/image-20220507172536383.png)

* VSCode上方会弹出下图所示的对话框，请选择Linux。

![image-20220507172610282](pic/image-20220507172610282.png)

* VSCode上方会弹出对话框，点击continue。

![image-20220507172653014](pic/image-20220507172653014.png)

* 输入密码，初始密码是：123456

![image-20220507172730763](pic/image-20220507172730763.png)

* 当左下角显示为SSH:IP地址，且为绿色的时候，说明已经和Ubuntu建立链接了。

![image-20220507172802843](pic/image-20220507172802843.png)

### 步骤9：导入Hi3516DV300的工程

* 首先点击左边的DevEco图标，然后点击DevEco Home下面的Home。

![image-20220502153726538](pic/image-20220502153726538.png)

* 点击import Project按钮

![image-20220502154202405](pic/image-20220502154202405.png)

* 然后再import Projet下面的路径，输入 /home/openharmony，然后再点击 import按钮。

![image-20220502154057893](pic/image-20220502154057893.png)

* 弹出对话框之后，再点击import。

![image-20220502154229102](pic/image-20220502154229102.png)

* 然后再点击import from OpenHarmony Source

![image-20220502154354992](pic/image-20220502154354992.png)

* 然后选择Product为：ipcamera_hispark_taurus_linux，然后ohos Ver选择为3.0。

![image-20220502154658674](pic/image-20220502154658674.png)

* 点击Current Windows

![image-20220502154716964](pic/image-20220502154716964.png)

![image-20220502154748632](pic/image-20220502154748632.png)

* 点击左边的Deveco图标，然后选择Projet Settings，然后点击toolchain，确保下面的setup上面是绿色的对钩，说明环境已经OK了，如果是红色的X，说明环境还有缺失，需要点击setup，下载编译时所需的工具。
* 最后点击build按钮，进行代码的编译。

![image-20220502154959427](pic/image-20220502154959427.png)

* 出现success的字样，说明已经编译成功了。如果编译失败，请先在对应的build.log文件中分析具体的问题。

![image-20220502161250176](pic/image-20220502161250176.png)

* <font color='RedOrange '>关于Hi3516DV300的烧录，可以参考《[Hi3516DV300的烧录步骤及注意事项](https://gitee.com/wgm2022/wu_guiming.gitee.io/blob/master/01%20%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA%E7%9B%B8%E5%85%B3/Hi3516DV300%E7%83%A7%E5%BD%95%E5%8F%8A%E6%B3%A8%E6%84%8F%E4%BA%8B%E9%A1%B9.md)》</font>

### 步骤10：导入Hi3861的工程

* 点击右上角的Configuration按钮

![image-20220502161505276](pic/image-20220502161505276.png)

* product选择 wifiiot_hispark_pegasus，然后再点击finish。

![image-20220502161539313](pic/image-20220502161539313.png)

* 点击toolchain，下拉Product，选择wifiiot_hispark_pegasus@hisilicon，我们可以看到，setup按钮上面显示的是红色的x，说明编译Hi3861时依赖的一些工具是缺失的，所以需要点击setup按钮，进行下载。

![image-20220502161743653](pic/image-20220502161743653.png)

* 工具下载完成之后，红色的×就会变成绿色的√，然后点击右上角的保存按钮。

![image-20220502162108350](pic/image-20220502162108350.png)

* 保存成功后，在会PROJET TASKS下方出现一个Hi3861工程的编译选项，点击build按钮，即可编译Hi3861的代码。

![image-20220502162228983](pic/image-20220502162228983.png)

* 当出现success的字样，说明hi3861的工程就编译成功了。

![image-20220502162437208](pic/image-20220502162437208.png)

* <font color='RedOrange '>关于Hi3861的烧录过程请参考《[Hi3861V100开发板烧录](https://gitee.com/link?target=https%3A%2F%2Fdevice.harmonyos.com%2Fcn%2Fdocs%2Fdocumentation%2Fguide%2Fide-hi3861-upload-0000001051668683)》</font>

### 附录：

* 文件拷贝

```shell
# 1、主机拷⻉⽂件到docker编译环境⾥：

sudo docker cp 源⽂件 openharmony:/⽬标⽂件
# 参数解析：
# 源⽂件：主机上的，可为⽂件或者⽬录
# ⽬标⽂件：docker编译环境⾥的，通常为⽬录，表示将⽂件拷⻉到该⽬录下

# 2、docker编译环境拷⻉⽂件到主机：

sudo docker cp openharmony:/源⽂件 ⽬标⽂件

# 源⽂件：docker编译环境⾥的，可为⽂件或者⽬录
# ⽬标⽂件：主机上的，通常为⽬录，表示将⽂件拷⻉到该⽬录下
```

* 删除docker编译环境【谨慎操作，不可恢复】

```shell
# 查看当前运⾏的docker实例状态
sudo docker ps -a

# 在上⼀条指显示结果列表中，查看openharmony的STATUS
# 如为 Up，则需要执⾏下⾯这条指令，停⽌其运⾏
# 如为 Exited，则跳过下⾯这条指令
sudo docker stop openharmony

# 删除
sudo docker rm openharmony
```

**<font color='RedOrange '>提醒：</font>**

**<font color='RedOrange '>删除前 ，请确保该运⾏环境内的有效数据都已拷⻉到主机上</font>**

**<font color='RedOrange '>删除后 ，该运⾏环境内的所有数据将被移除，不可恢复</font>**

