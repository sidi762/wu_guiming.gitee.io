# 【Hi3516DV300】如何让sample实现开机自启动

* 由于指导文档都是需要通过在VScode的monitor输入指定的命令，对应的sample才能在开发板上面运行，很多同学希望了解如何让开发板一上电之后，开发板就能自己输出指定的命令，不需要自己重新用VScode的monitor来输入命令，sample就能自动运行。



## 系统初始化

* 在开发板的/etc/文件夹中包含rcS脚本，Sxxx脚本和fstab脚本。init进程在启动系统服务之前执行这些脚本。执行的流程为“rcS->fstab->S00-xxx“。Sxxx脚本中的内容与开发板和产品需要有关，主要包括设备节点的创建、创建目录、扫描设备节点、修改文件权限等等

* 如果我们想让开发板一上电就能运行我们指定的命令，我们可以通过修改系统初始化文件来实现，我们主要修改的是文件 /etc/init.d/S82ohos。



**具体的步骤如下**：

## 步骤1：熟悉如何在Hi3516DV300开发板中使用 busybox vi命令来修改文本文件的内容

* 参考文档《[如何在Hi3516DV300开发板终端使用vi命令](https://gitee.com/wgm2022/wu_guiming.gitee.io/blob/master/01%20%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA%E7%9B%B8%E5%85%B3/%E5%A6%82%E4%BD%95%E5%9C%A8Hi3516DV300%E5%BC%80%E5%8F%91%E6%9D%BF%E7%BB%88%E7%AB%AF%E4%BD%BF%E7%94%A8vi%E5%91%BD%E4%BB%A4.md)》拷贝busybox至开发板，并熟悉busybox vi 命令的使用方法。



## 步骤2：复制您想一上电就运行的那个sample的依赖文件至开发板

* 无论如何，您如果想运行某个sample，您都需要把该sample的可执行程序以及他依赖的库文件，资源文件等全部复制到开发板的对应目录下，这个请参考对应sample目录下的指导文档。
* 本文以HelloWorld为例进行演示，我首先按照[《 helloworld操作指导》](https://gitee.com/openharmony/device_soc_hisilicon/blob/master/hi3516dv300/sdk_linux/sample/taurus/helloworld/README.md)文档的步骤，把ohos_helloworld_demo、libvb_server.so、 libmpp_vbs.so这三个文件通过SD卡或者nfs挂载的方式拷贝到开发板的对应目录下。
* 然后按照[《 helloworld操作指导》](https://gitee.com/openharmony/device_soc_hisilicon/blob/master/hi3516dv300/sdk_linux/sample/taurus/helloworld/README.md)文档的步骤，执行一遍命令，确保我们后续开机自启动的程序，在运行的时候不存在问题，大致的命令如下

```
insmod /ko/hi_mipi_tx.ko

cd /userdata

./ohos_helloworld_demo
```



## 步骤3：修改系统初始化文件S82ohos

* 确认好我们在开机自启动时，需要执行的命令之后，我们就可以把这些命令添加到系统初始化文件S82ohos文本中了。
* 执行下面的命令，j将busybox vi命令重命名为vi

```
alias vi='busybox vi'
```

* 执行下面的命令，打开 /etc/init.d/S82ohos

```
vi /etc/init.d/S82ohos
```

![image-20220524160746871](pic/image-20220524160746871.png)

* 按键盘的i键，进入编辑模式，把需要开机自启动的几条命令添加到文件中

![image-20220524161010036](pic/image-20220524161010036.png)

* 按键盘的Esc键，退出编辑模式进入命令行模式，输入:wq 敲回车，保存并退出

![image-20220524161224377](pic/image-20220524161224377.png)

* 执行下面的命令，查看一下文件中的内容是否添加正确

```
cat  /etc/init.d/S82ohos
```

![image-20220524161328319](pic/image-20220524161328319.png)



## 步骤4：重启开发板，查看现象

* 重启开发板，看系统是否自动运行了我们添加的那几条命令
* 通过系统的打印信息可以看出，开发板一上电，HelloWorld sample就能自己运行了，且LCD屏幕也能正常工作。

![image-20220524161850797](pic/image-20220524161850797.png)



* 到此，如何让sample实现开机自启动的文档就结束了。