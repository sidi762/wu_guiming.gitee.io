# 2022年嵌入式竞赛海思赛道冲刺阶段注意事项

各位亲爱的参赛选手：

   在临近作品提交之际，鼓励大家竭尽全力、奋勇向前，充分利用好剩下的暑假时间努力完成一份完美的答卷。

如下是一些注意事项：

1、海思鼓励每支参赛队伍积极提交作品，并尽可能为参赛队伍争取成绩。如果在提交时没能成功实现功能演示，请按照《[作品报告模板](http://www.socchina.net/download)（须电脑访问》书写出作品方案设计文档，并基于作品设计思路及对比赛套件的了解，在电脑上制作一段讲解的视频。

2、每支队伍须填写《2022嵌入式竞赛海思赛道作品进度及信息反馈》，反馈**是否计划在7月14日18:00前提交作品**等。

**填写方法**：参赛队长用微信扫描下述二维码，选择“修改上次提交信息”-“载入上一次回答”，每支队伍只须填写一份，且仅保留一份提交。

![1656929316929](../01%20环境搭建相关/pic/1656929316929.png)

3、**作品提交注意事项**
* 加入嵌入式竞赛**组委会官方QQ群**，如下选其一：一群：575699069，二群：774888694，三群：690977169，关注群内提交要求。队长加入2022嵌入式队长群：653704318。
* 作品代码需要开源，方法是上传**至OpenHarmony-sig社区**，成为OpenHarmony贡献者(**无需上传至vendor_hihope或者HiSpark_NICU2022仓**)。参考《[社区代码提交流程简介](https://gitee.com/openharmony-sig/contest/blob/master/2022-hisilicon-national-embedded-competition/README_zh.md)》，把**项目工程代码（仅需自己修改和实现的目录文件）、作品设计报告、README.md**上传。在README.md里填写作品简介。开源操作如有问题，请咨询海思官方QQ群（群号：1085433752）中的：OpenHarmony_知识体系_张宇航(1342818227)。
* 按照组委会《[2022嵌入式竞赛应用赛道作品上传要求](http://www.socchina.net/file/cacheFile/f12d77a4f75d4fe8a862fc1f95f0c5d7.pdf)》与《[2022嵌入式竞赛应用赛道作品报告模板](http://www.socchina.net/file/cacheFile/a7fdc40adf8f44d7a85d2329412edd65.docx)》，把**作品制作视频、作品设计报告、关键代码打包**，分别上传至组委会官网。另外，在“龙芯、海思赛道代码仓库地址”填写上述在OpenHarmony-sig社区的作品代码开源链接。在组委会官网提交作品如有问题，请在组委会官方QQ群咨询。
* 作品成功提交后，联系海思官方QQ群：海思陈雨豪(793952520)，留言格式为：“团队名称+作品名称已提交，请帮忙检查”，海思将协助检查提交的资料是否符合要求。

4、**2021年部分作品提交案例**，仅做参考：
* 百度网盘链接：https://pan.baidu.com/s/1CYE7bQepj17OJPh82iLWTg?pwd=yisd
* 阿里云盘链接：https://www.aliyundrive.com/s/BATeGFQiasj 提取码: mf17

5、注意整理和备份作品代码及文档成果，推荐复赛及决赛答辩时做好作品演示备份方案。比赛时间见《[2022年全国大学生嵌入式芯片与系统设计竞赛的通知](http://www.socchina.net/details?id=a1ebfc839fa64f18ac80c8a503e4ed70)》

6、嵌入式竞赛线上交流社区网址为：<https://gitee.com/HiSpark/HiSpark_NICU2022>，内有参赛学习路径、FAQ、可在Issues中搜索和反馈问题。

7、**回收比赛套件的邮寄地址**及方法，在7月14日提交作品之后公布，请留意通知。

​**注：本注意事项会持续更新，请保存至浏览器书签**   
​                                                                 

​                                                                                                                                           2022年7月13日